#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<locale.h>

#define TAM 150
#define MAXSTR 15000

#define true 1
#define false 0

typedef int bool;
char look;

FILE *abreArq(char *nome, char *modo);
void p2p5(FILE *In, FILE *Out, int lin, int col, int p);
void p5p2(FILE *In, FILE *Out, int lin, int col, int p);
void header(char *tipo, int *lin, int *col, int *p, FILE *f);
void skypWhite(FILE *p);
void nextChar(FILE *p);
void newLine(FILE *p);
int getNum(FILE *p);
void getNome(FILE *p, char *nome);

/*********************************************************
 *
 *********************************************************/
int main(int argc, char *argv[])
{
	FILE *In, *Out;
	int lin, col, p;
	char tipo[5];
	char nomeIn[50], nomeOut[50];
    setlocale(LC_ALL,"");
    //fpos_t position;
    //long tam;

	if(argc!=3)
	{
		printf("Digite o nome do arquivo e entrada: ");
		scanf("%s",nomeIn);
		printf("Digite o nome do arquivo de sa�da: ");
		scanf("%s",nomeOut);


	}else
	{
		strcpy(nomeIn,argv[1]);
		strcpy(nomeOut,argv[2]);
	}
  //
	In  = fopen(nomeIn, "rb");
	if( In == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s",nomeIn);
		exit(1);
	}
	Out = fopen(nomeOut, "wb");
	if( Out == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s",nomeOut);
		exit(1);
	}

	//
	header(tipo, &lin, &col, &p, In);


	if(strcmp(tipo,"P2")==0)
	{
		fprintf(Out,"P5\n");
		fprintf(Out,"# arquivo gerando pela ferramenta P2P5\n");
		fprintf(Out,"%d %d\n",col,lin);
		fprintf(Out,"%d\n",p);
		p2p5(In, Out, lin, col, p);
	}else
	{
		fprintf(Out,"P2\n");
		fprintf(Out,"# arquivo gerando pela ferramenta P2P5\n");
		fprintf(Out,"%d %d\n",col,lin);
		fprintf(Out,"%d\n",p);
		p5p2(In, Out, lin, col, p);
	}

	fclose(In);
	fclose(Out);
    return 0;
}

/*********************************************************
 *
 *********************************************************/
FILE *abreArq(char *nome, char *modo)
{
	FILE *p;
	p = fopen(nome, modo);
	if( p == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s",nome);
		exit(1);
	}
	return p;
}

/*********************************************************
 *
 *********************************************************/
void p2p5(FILE *In, FILE *Out, int lin, int col, int p)
{
  int i, j, *v;
	char var[4]={'\0','\0','\0','\0'};
	v = (int*) var;
	//long tam = ftell(Out);
	//rewind(Out);
	//fseek (Out, tam-1,SEEK_SET);
	nextChar(In);
	for(i=0; i<lin; i++)
	{
		skypWhite(In);
		if(i>0)
			newLine(In);
		for(j=0; j<col; j++)
		{
			skypWhite(In);
			if(!isdigit(look))
			{
			  printf("arquivo Inv�lido, esperado um numero na linha %d coluna %d\n",lin,col);
			}
			*v = getNum(In);
			if(p>255)
			{
			  fputc(var[1] , Out);
			  fputc(var[0] , Out);
			}else
			{
			  fputc(*v , Out);
			  //printf("0x%02X\n",*v);
			}
		}
	}
}

/*********************************************************
 *
 *********************************************************/
void skypWhite(FILE *p)
{
	while(look == ' ' || look == '\t' )
		nextChar(p);
}

/*********************************************************
 *
 *********************************************************/
void newLine(FILE *p)
{
	while(look != '\n' && look != '\0' )
		nextChar(p);
	if(look == '\n' || look == '\0')
	  nextChar(p);
	if(look == '\n' || look == '\0')
	  nextChar(p);
}

/*********************************************************
 *
 *********************************************************/
void nextChar(FILE *p)
{
  look = fgetc(p);
}

/*********************************************************
 *
 *********************************************************/
int getNum(FILE *p)
{
	char var[18];
	int i=0;
	while(isdigit(look))
	{
		var[i] = look;
		i++;
		nextChar(p);
	}
	var[i]='\0';
	return atoi(var);
}

/*********************************************************
 *
 *********************************************************/
void getNome(FILE *p, char *nome)
{
	int i=0;
	while(isalpha(look))
	{
		nome[i] = look;
		i++;
		nextChar(p);
	}
	nome[i]='\0';
}

/*********************************************************
 *
 *********************************************************/
void p5p2(FILE *In, FILE *Out, int lin, int col, int p)
{
    int i, j, *v;
	char var[4]={'\0','\0','\0','\0'};
	v = (int*) var;
	/*long tam = ftell(Out);
	rewind(Out);
	fseek (Out, tam-1,SEEK_SET); */
	nextChar(In);
	for(i=0; i<lin; i++)
	{
		for(j=0; j<col; j++)
		{
			if(p>255)
			{
				var[1] = look;
				nextChar(In);
			  var[0] = look;
				nextChar(In);
				var[2]='\0';
				if(j==0)
				  fprintf(Out,"%d", *v);
				else
				  fprintf(Out," %d", *v);
			}else
			{
			  var[0] = look;
				var[1]='\0';
				if( j==0)
				  fprintf(Out,"%d", *v);
			  else
				  fprintf(Out," %d", *v);
				nextChar(In);
			}
		}
		fprintf(Out,"\n");
	}
}

/*********************************************************
 *
 *********************************************************/
void header(char *tipo, int *lin, int *col, int *p, FILE *f)
{
    int tam;
	char linha[TAM];
	//char var[TAM];
	// pegando P2
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	printf("\n%s\n",linha);
	//printf("Tam: %d  str: %d\n",tam,strlen(linha));
	if(strcmp(linha,"P2")!=0 && strcmp(linha,"P5")!=0)
	{
		printf("Erro ao ler o P2: arquivo com formato inv�lido");
		exit(1);
	}
	strcpy(tipo,linha);
	// pegando a linha coment�rio
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	printf("%s\n",linha);
	if(linha[0]!='#')
	{
		printf("Comentario n�o encontrado na linha 2: arquivo com formato inv�lido");
		exit(1);
	}
	// pegnado linha e coluna
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	if(sscanf(linha,"%d %d",col, lin) !=2)
	{
		printf("arquivo com formato inv�lido");
		exit(1);
	}
	printf("linha: %d\nCpluna: %d\n",*lin,*col);
	// pegando o valor maxino do pixel
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	if( sscanf(linha,"%d",p) != 1)
	{
		printf("arquivo com formato inv�lido");
		exit(1);
	}
	printf("Valor maxino do pixel: %d\n",*p);
}
